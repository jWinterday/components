import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SlideMenu extends StatefulWidget {
  SlideMenu({
    key,
    @required this.child,
    this.menu,
    this.menuItems,
    this.slideWidth = 0.25,
    this.spaceColor = Colors.orange,
    this.slideThreshold = 0.1,
    this.slideTimeAnimation = 200,
    this.onDragEnd,
    this.onDragStart,
  })  : assert(child != null),
        assert(menuItems != null);

  final Widget child;
  final Widget menu;
  final List<Widget> menuItems;
  final double slideWidth;
  final Color spaceColor;
  final double slideThreshold;
  final int slideTimeAnimation;
  final VoidCallback onDragEnd;
  final VoidCallback onDragStart;

  @override
  _SlideMenuState createState() => _SlideMenuState();
}

class _SlideMenuState extends State<SlideMenu>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<Offset> _animation;
  static _SlideMenuState _currentAnimatedState;

  @override
  initState() {
    super.initState();

    _controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: widget.slideTimeAnimation));

    _animation = Tween(begin: Offset.zero, end: Offset(-widget.slideWidth, 0.0))
        .animate(_controller);
  }

  @override
  dispose() {
    _controller.dispose();
    if (_currentAnimatedState == this) {
      _currentAnimatedState = null;
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (data) {
        setState(() {
          _controller.value -= data.primaryDelta / context.size.width;
        });

        _currentAnimatedState?._controller?.animateTo(0.0);
      },
      onHorizontalDragEnd: (data) {
        if (data.primaryVelocity > 2500) {
          _controller.animateTo(0.0);
        } else if (_controller.value >= widget.slideThreshold ||
            data.primaryVelocity < -2500) {
          _controller.animateTo(1.0);

          _currentAnimatedState = this;
        } else {
          _controller.animateTo(0.0);
        }
      },
      child: Stack(
        children: <Widget>[
          SlideTransition(position: _animation, child: widget.child),
          Positioned.fill(
            child: LayoutBuilder(
              builder: (context, constraint) {
                double menuWidth = widget.slideWidth * constraint.maxWidth;

                return AnimatedBuilder(
                  animation: _controller,
                  builder: (context, child) {
                    return Stack(
                      children: <Widget>[
                        Positioned(
                          right: -menuWidth -
                              constraint.maxWidth * _animation.value.dx,
                          top: 0.0,
                          bottom: 0.0,
                          width: menuWidth,
                          child: Container(
                            color: widget.spaceColor,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: widget.menuItems.map((child) {
                                return Expanded(
                                  child: child,
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
